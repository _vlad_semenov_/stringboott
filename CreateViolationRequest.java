package com.example.SPRINGBOOTTTT;
import lombok.Data;
import lombok.experimental.Accessors;
@Data
@Accessors(chain = true)
public class CreateViolationRequest {
    private Integer id;
    private Integer number_of_the_car;
    private String Full_name_of_the_violator;
    private String name_of_the_traffic_cop;
    private String who_wrote_the_protocol;
    private Integer time_of_the_protocol;
    private Integer fine_amount;
}
