package com.example.SPRINGBOOTTTT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringboottttApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringboottttApplication.class, args);

	}
}
