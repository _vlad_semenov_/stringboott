package com.example.SPRINGBOOTTTT;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.tomcat.jni.Address;

import javax.persistence.*;
import java.util.Objects;
@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "Violations")
public class violations {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "number_of_the_car")
    private Integer number_of_the_car;

    @Column(name = "Full_name_of_the_violator")
    private String Full_name_of_the_violator;

    @Column(name = "name_of_the_traffic_cop")
    private String name_of_the_traffic_cop;

    @Column(name = "who_wrote_the_protocol")
    private String who_wrote_the_protocol;

    @Column(name = "time_of_the_protocol")
    private Integer time_of_the_protocol;

    @Column(name = "fine_amount")
    private Integer fine_amount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "number_of_the_car_id", foreignKey = @ForeignKey(name = "fk_Violations_number_of_the_car_id"))
    private Address address;

    @Override
    public int hashCode() {
        return Objects.hash(number_of_the_car);
    }

    @Override
    public String toString() {
        return "Violations{" +
                "id=" + id +
                ", number_of_the_car='" + number_of_the_car + '\'' +
                ", Full_name_of_the_violator'" + Full_name_of_the_violator + '\'' +
                ", name_of_the_traffic_cop'" + name_of_the_traffic_cop + '\'' +
                ", who_wrote_the_protocol'" + who_wrote_the_protocol + '\'' +
                ", time of the protocol=" + time_of_the_protocol +'\'' +
                ", fine_amount'" + fine_amount +
                '}';
    }
}
